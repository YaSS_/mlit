package com.skillup.sergeyyaskevich.motionlayoutit

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.support.constraint.motion.MotionLayout
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import com.skillup.sergeyyaskevich.motionlayoutit.ui.BankActivity
import kotlinx.android.synthetic.main.activity_main.*

class ViewPagerHeader @JvmOverloads internal constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MotionLayout(context, attrs, defStyleAttr), ViewPager.OnPageChangeListener/*, AppBarLayout.OnOffsetChangedListener*/ {

    val gradient = GradientDrawable(
        GradientDrawable.Orientation.TL_BR,
        intArrayOf(Color.parseColor("#FFD295ED"), Color.parseColor("#FFEF2D74"))
    );

    init {
        background = gradient
    }

    override fun onPageScrollStateChanged(p0: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        val pageSize = 2
        val prog = (position + positionOffset) / (pageSize - 1)

        progress = prog

        val mainActivity = context as BankActivity
        mainActivity.mainMotionLayout.progress = progress
    }

    override fun onPageSelected(p0: Int) {
    }
}