package com.skillup.sergeyyaskevich.motionlayoutit.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.skillup.sergeyyaskevich.motionlayoutit.R

private const val ITEM_COUNT = 15

class ListAdapter : RecyclerView.Adapter<ListAdapter.ItemVH>() {

    private var inflater: LayoutInflater? = null

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ItemVH {
        return ItemVH(getInflater(container.context).inflate(R.layout.item_list, container, false))
    }

    override fun getItemCount() = ITEM_COUNT

    override fun onBindViewHolder(holder: ItemVH, p1: Int) {
    }

    private fun getInflater(context: Context): LayoutInflater {
        return inflater ?: LayoutInflater.from(context).also {
            inflater = it
        }
        View.GONE
    }

    class ItemVH(view: View) : RecyclerView.ViewHolder(view) {}
}