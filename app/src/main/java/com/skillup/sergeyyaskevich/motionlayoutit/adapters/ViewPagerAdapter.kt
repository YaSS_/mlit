package com.skillup.sergeyyaskevich.motionlayoutit.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.skillup.sergeyyaskevich.motionlayoutit.ui.Page

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return Page()
    }

    override fun getCount() = 2
}