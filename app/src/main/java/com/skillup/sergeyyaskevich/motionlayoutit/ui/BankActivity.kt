package com.skillup.sergeyyaskevich.motionlayoutit.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.motion.MotionLayout
import android.support.v4.view.ViewPager
import com.skillup.sergeyyaskevich.motionlayoutit.R
import com.skillup.sergeyyaskevich.motionlayoutit.adapters.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class BankActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val motionLayout = findViewById<MotionLayout>(R.id.viewPagerHeader)

        var viewPagerAdapter =
            ViewPagerAdapter(supportFragmentManager)
        if (motionLayout != null) {
            viewPager.addOnPageChangeListener(motionLayout as ViewPager.OnPageChangeListener)
        }
        viewPager.adapter = viewPagerAdapter
    }
}
