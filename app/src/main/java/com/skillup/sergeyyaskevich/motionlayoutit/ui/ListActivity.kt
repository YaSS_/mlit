package com.skillup.sergeyyaskevich.motionlayoutit.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.skillup.sergeyyaskevich.motionlayoutit.R
import com.skillup.sergeyyaskevich.motionlayoutit.adapters.ListAdapter
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        rv_list.adapter = ListAdapter()
    }
}