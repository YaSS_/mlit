package com.skillup.sergeyyaskevich.motionlayoutit.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.skillup.sergeyyaskevich.motionlayoutit.R
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        btn_Bank.setOnClickListener(this@MenuActivity)
        btn_list.setOnClickListener(this@MenuActivity)
    }

    override fun onClick(v: View?) {
        v?.let {
            val intent = when (it.id) {
                R.id.btn_Bank -> Intent(this@MenuActivity, BankActivity::class.java)
                R.id.btn_list -> Intent(this@MenuActivity, ListActivity::class.java)
                else -> Intent()
            }

            startActivity(intent)
        }
    }


}